//START DB
//†These simple weapons do not require any skill training to use effectively. Characters wishing to specialize, learning additional techniques with these simple weapons, will require the skill in order to further their training.

var combatskills_db = TAFFY([
     {id:1, name:'Aim', cost:4, prereq:'Weapon Skill', notutor:3, enabled:true, description:''},
     {id:2, name:'Ambidexterity', cost:4, prereq:'Two Weapons', notutor:3, enabled:true, description:''},
     {id:3, name:'Assassinate', cost:20, prereq:'Vital Blow', notutor:3, enabled:true, description:''},
     {id:4, name:'Backstab', cost:5, prereq:'Weapon Skill', notutor:3, enabled:true, description:''},
     {id:5, name:'Blinding Blow', cost:15, prereq:'Disable Limb', notutor:3, enabled:true, description:''},
     {id:6, name:'Channel Spell', cost:15, prereq:'Weapon Mastery, 10 SE or ME', notutor:3, enabled:true, description:''},
     {id:7, name:'Counter Attack', cost:4, prereq:'Weapon Skill', notutor:3, enabled:true, description:''},
     {id:8, name:'Critical Attack', cost:10, prereq:'Backstab', notutor:3, enabled:true, description:''},
     {id:9, name:'Decapitate', cost:20, prereq:'Sever & Power Attack', notutor:3, enabled:true, description:''},
     {id:10, name:'Disable Limb', cost:8, prereq:'Subdue & Backstab', notutor:3, enabled:true, description:''},
     {id:11, name:'Disarm', cost:6, prereq:'Weapon Mastery', notutor:3, enabled:true, description:''},
     {id:12, name:'Dodge', cost:10, prereq:'', notutor:'N', enabled:true, description:''},
     {id:13, name:'Florentine', cost:4, prereq:'Weapon Skill', notutor:3, enabled:true, description:''},
     {id:14, name:'Fury', cost:15, prereq:'Rage', notutor:3, enabled:true, description:''},
     {id:15, name:'Garrote', cost:8, prereq:'Backstab (any weapon type)', notutor:'N', enabled:true, description:''},
     {id:17, name:'Knockback', cost:8, prereq:'Weapon Mastery, at least Exceptional Strength 2', notutor:'N', enabled:true, description:''},
     {id:18, name:'Knockout', cost:6, prereq:'Backstab', notutor:3, enabled:true, description:''},
     {id:19, name:'Lethal Hands', cost:2, prereq:'Hand to Hand', notutor:2, enabled:true, description:''},
     {id:20, name:'Master Dodge', cost:20, prereq:'Dodge', notutor:'N', enabled:true, description:''},
     {id:21, name:'Muting Blow', cost:10, prereq:'Disable Limb', notutor:3, enabled:true, description:''},
     {id:22, name:'Parry', cost:5, prereq:'Counter Attack', notutor:3, enabled:true, description:''},
     {id:23, name:'Power Attack', cost:10, prereq:'Weapon Mastery', notutor:3, enabled:true, description:''},
     {id:24, name:'Rage', cost:4, prereq:'Weapon Skill', notutor:3, enabled:true, description:''},
     {id:25, name:'Ranged Pin', cost:6, prereq:'Weapon Mastery in Ranged', notutor:3, enabled:true, description:''},
     {id:26, name:'Resist Movement', cost:4, prereq:'Shield, Physical Prowess', notutor:3, enabled:true, description:''},
     {id:27, name:'Return Blow', cost:10, prereq:'Disarm, Parry', notutor:3, enabled:true, description:''},
     {id:28, name:'Ringing Blow', cost:8, prereq:'Subdue', notutor:3, enabled:true, description:''},
     {id:29, name:'Sever', cost:15, prereq:'Weapon Mastery', notutor:3, enabled:true, description:''},
     {id:30, name:'Shatter', cost:6, prereq:'Power Attack & at least Exceptional Strength 2', notutor:3, enabled:true, description:''},
     {id:31, name:'Shield', cost:4, prereq:'Physical Prowess for large shields', notutor:'N', enabled:true, description:''},
     {id:32, name:'Slay', cost:15, prereq:'Power Attack', notutor:3, enabled:true, description:''},
     {id:33, name:'Staggering Blow', cost:12, prereq:'Ringing Blow', notutor:3, enabled:true, description:''},
     {id:34, name:'Stunning Blow', cost:10, prereq:'Subdue', notutor:3, enabled:true, description:''},
     {id:35, name:'Subdue', cost:4, prereq:'Weapon Mastery', notutor:3, enabled:true, description:''},
     {id:36, name:'Sustaining Fury', cost:8, prereq:'Fury, 10LP', notutor:3, enabled:true, description:''},
     {id:37, name:'Two Weapons', cost:6, prereq:'Florentine', notutor:3, enabled:true, description:''},
     {id:38, name:'Vital Blow', cost:15, prereq:'Critical Attack', notutor:3, enabled:true, description:''},
     {id:39, name:'Weapon Mastery', cost:10, prereq:'Counter Attack or Aim', notutor:3, enabled:true, description:''},
     {id:40, name:'Wear Heavy Armor', cost:2, prereq:'Wear Light Armour & Physical Prowess', notutor:'N', enabled:true, description:''},
     {id:41, name:'Wear Light Armor', cost:1, prereq:'', notutor:'N', enabled:true, description:''},
     {id:42, name:'Wound', cost:8, prereq:'Critical Attack', notutor:3, enabled:true, description:''}
]);

var weaponskills_db = TAFFY([
     {id:1, name:'1-Handed Blunt†', cost:3, prereq:'', notutor:2, enabled:true, description:''},
     {id:2, name:'1-Handed Sword', cost:5, prereq:'', notutor:2, enabled:true, description:''},
     {id:3, name:'2-Handed Blunt', cost:6, prereq:'Physical Prowess', notutor:2, enabled:true, description:''},
     {id:4, name:'2-Handed Sword', cost:8, prereq:'Physical Prowess', notutor:2, enabled:true, description:''},
     {id:5, name:'Axe', cost:8, prereq:'', notutor:2, enabled:true, description:''},
     {id:6, name:'Bow', cost:4, prereq:'', notutor:2, enabled:true, description:''},
     {id:7, name:'Crossbow', cost:4, prereq:'', notutor:'N', enabled:true, description:''},
     {id:8, name:'Exotic (type)', cost:8, prereq:'', notutor:3, enabled:true, description:''},
     {id:9, name:'Hand to Hand†', cost:2, prereq:'', notutor:'N', enabled:true, description:''},
     {id:10, name:'Improvised Weapons†', cost:2, prereq:'', notutor:'N', enabled:true, description:''},
     {id:11, name:'Knife†', cost:2, prereq:'', notutor:'N', enabled:true, description:''},
     {id:12, name:'Pole Arm†', cost:8, prereq:'Physical Prowess', notutor:2, enabled:true, description:''},
     {id:13, name:'Sling†', cost:4, prereq:'', notutor:'N', enabled:true, description:''},
     {id:14, name:'Spear', cost:3, prereq:'', notutor:2, enabled:true, description:''},
     {id:15, name:'Staff†', cost:4, prereq:'', notutor:'N', enabled:true, description:''},
     {id:16, name:'Thrown Weapon', cost:4, prereq:'', notutor:'N', enabled:true, description:''}
]);

//not done
var academic_db = TAFFY([
     {id:1, name:'Appraising', cost:4, prereq:'', notutor:3, enabled:true, description:''},
     {id:2, name:'Arcane Research', cost:'2+lvl', prereq:'Magery', notutor:3, enabled:true, description:''},
     {id:3, name:'Astrology', cost:6, prereq:'Read & Write', notutor:3, enabled:true, description:''},
     {id:4, name:'Catalyst', cost:5, prereq:'Magery', notutor:2, enabled:true, description:''},
     {id:5, name:'Clerical Investment', cost:'3xlvl', prereq:'Theology', notutor:3, enabled:true, description:''},
     {id:6, name:'Concentration', cost:5, prereq:'Magery', notutor:2, enabled:true, description:''},
     {id:7, name:'Devotion', cost:6, prereq:'Clerical Investment 7/8', notutor:2, enabled:true, description:''},
     {id:8, name:'Divination', cost:'3xlvl', prereq:'Astrology', notutor:3, enabled:true, description:''},
     {id:9, name:'Draw Essence', cost:10, prereq:'Magery or CI:1', notutor:2, enabled:true, description:''},
     {id:10, name:'Expanded Domain', cost:7, prereq:'Cl', notutor:2, enabled:true, description:''},
     {id:11, name:'Magery', cost:6, prereq:'', notutor:'N', enabled:true, description:''},
     {id:12, name:'Master of Divination', cost:3, prereq:'Physical Prowess', notutor:2, enabled:true, description:''},
     {id:13, name:'Magical Energy', cost:'2/pt', prereq:'', notutor:'N', enabled:true, description:''},
     {id:14, name:'Prayer', cost:6, prereq:'', notutor:2, enabled:true, description:''},
     {id:15, name:'Read & Write', cost:6, prereq:'', notutor:'N', enabled:true, description:''},
     {id:16, name:'Read Magic', cost:6, prereq:'', notutor:'N', enabled:true, description:''},
     {id:17, name:'Spell Specialization', cost:6, prereq:'', notutor:'N', enabled:true, description:''},
     {id:18, name:'Spiritual Energy', cost:'2/pt', prereq:'', notutor:'N', enabled:true, description:''},
     {id:19, name:'Syphon', cost:10, prereq:'', notutor:'N', enabled:true, description:''},
     {id:20, name:'Teaching', cost:3, prereq:'', notutor:'N', enabled:true, description:''},
     {id:21, name:'Theology', cost:4, prereq:'', notutor:'N', enabled:true, description:''},
     {id:22, name:'Trade Adept', cost:4, prereq:'', notutor:'N', enabled:true, description:''},
     {id:23, name:'Trade Master', cost:10, prereq:'', notutor:'N', enabled:true, description:''}
]);

//not done
var abilities_db = TAFFY([
     {id:1, name:'1-Handed Sword', cost:3, prereq:'', notutor:2, enabled:true, description:''},
     {id:2, name:'2-Handed Sword', cost:8, prereq:'', notutor:3, enabled:true, description:''}
]);

//not done
var tradeskills_db = TAFFY([
     {id:1, name:'1-Handed Sword', cost:3, prereq:'', notutor:2, enabled:true, description:''},
     {id:2, name:'2-Handed Sword', cost:8, prereq:'', notutor:3, enabled:true, description:''}
]);
// END DBs

$(document).ready(function() {
	$('#nav-container').load('header.html #nav');
});

function generateCombatSkills() {    
     var data = combatskills_db().get({});
     
     var html = '<table data-toggle="table">'; 
     	 html+= '\n\t<thead>';
     	 html+= '\n\t\t<tr>';
     	 html+= '\n\t\t\t<th>ID</th>';
     	 html+= '\n\t\t\t<th>Name</th>';
     	 html+= '\n\t\t\t<th>Cost</th>';
     	 html+= '\n\t\t\t<th>Tutor</th>';
     	 html+= '\n\t\t\t<th>Prereqs</th>';
     	 html+= '\n\t\t\t<th>Description</th>';
     	 html+= '\n\t\t</tr>';
     	 html+= '\n\t</thead>';
     	 html+= '\n\t<tbody>';

     for (var key in data)
     {
          // var imageUrl = "img/portfolio/" + data[key].thumbUrl;

          if(!data.hasOwnProperty(key)) {
               continue;
          }

          // if (data[key].thumbUrl == "")      
          //      imageUrl = "img/portfolio/missing.png";
          
          html += '<div class="container combatSkillItem">';
          // html += '\n\t<h2>' + data[key].name + '</h2>';
          html += '\n\t<button type="button" class="btn btn-info" data-toggle="collapse" data-target="' + data[key].id + '">' + data[key].name + '</button>';          
          html += '\n\t<div id="' + data[key].id + '" class="collapse">';
          html += '\n\t\t' + data[key].description;
          html += '\n\t</div>';
          html += '\n</div>';          
     }

     html += '\n\t</tbody>';
     html += '\n</table>';

     return html;
  }